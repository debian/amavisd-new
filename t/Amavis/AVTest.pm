# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::AVTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::AV' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  return 'Incomplete module';
  use_ok $test->class;
}

sub empty : Tests(0) {
}

1;
